const express = require("express");
const router = express.Router();
const controller = require("../controllers");
const multer = require("multer");
const path = require("path");

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, path.join(__dirname,"../uploads"));
  },
  filename: function(req, file, cb) {
    cb(null, file.fieldname + "-" + Date.now() + ".jpg");
  }
});
const upload = multer({ storage: storage });

router.get("/get-all-offer", controller.getAllOffers)

router.get("/get-offer/:id", controller.getOffer)

router.post("/add-offer", upload.single("picture"), controller.addOffer)

router.put("/update-offer/:id", upload.single("picture"), controller.updateOffer)

router.delete("/delete-offer/:id", controller.deleteOffer);

module.exports = router;