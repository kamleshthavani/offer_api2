const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const routes = require("./routes");
const mongoose = require("mongoose");
const path = require("path");
const constants = require("./controllers/constant");

var options = { keepAlive: 1, useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true };

mongoose
    .connect(`${constants.DB_PROTOCOL}://${constants.DB_HOST}:${constants.DB_PORT}/${constants.DB_NAME}`, options)
    .then(() => console.log("database connected"))
    .catch(() => console.log("Database not connected"));

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use("/uploads", express.static(path.join(__dirname,"/uploads")));

app.use("/api/", routes);


app.listen(3000, () => {
    console.log(`Server Listening on port ${3000}`);
});
