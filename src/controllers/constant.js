module.exports  = {
    ERR_GET_DATA: "Having problem while getting offer.",
    ERR_ADD_DATA: "Having problem while adding offer.",
    ERR_UPDATE_DATA: "Having problem while updating offer.",
    ERR_REMOVE_DATA: "Having problem while removing offer.",
    UPLOAD_IMAGE: "Please uplad image",
    ADD_SUCCESS: "Offer Added Successfully",
    UPDATE_SUCCESS: "Offer Updated Successfully",
    REMOVE_SUCCESS: "Offer Removed Successfully",
    SERVER_ERROR: "Server Error",
    PORT: 3000,
    DB_PROTOCOL:"mongodb",
    DB_HOST: "localhost",
    DB_PORT: "27017",
    DB_NAME: "offrs_db"
}