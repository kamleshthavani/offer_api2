const Offer = require("../models/offer");
const constants = require("./constant");
const fs = require("fs");
const path = require("path");

exports.getAllOffers = async (req, res, next) => {
    try{
        let offers = await Offer.find({}).lean().catch(e => {
            return res.json({code: 4, message: constants.ERR_GET_DATA })
        });
        return res.status(200).json({code: 5, offers: offers })
    }
    catch(e) {
        return res.json({error: constants.SERVER_ERROR})
    }
}

exports.addOffer = async (req, res, next) => {
    try{
        if(req.file == undefined) 
            return res.json({code:4, message: constants.UPLOAD_IMAGE});

        const offer = new Offer({
            offer: req.body.offer,
            picture: req.file.filename
        });

        offer.save((error, result) => {
            if(error) {
                console.log(error);
                return res.json({code:4, message: constants.ERR_ADD_DATA});
            }
            return res.status(200).json({code:5, message: constants.ADD_SUCCESS});
        });
    }
    catch(e) {
        return res.json({error: constants.SERVER_ERROR})
    }
}

exports.getOffer = async (req, res, next) => {
    try{
        let offer = await Offer.findOne({_id:req.params.id}).lean().catch(e => {
            return res.json({code: 4, message: constants.ERR_GET_DATA })
        });
        return res.status(200).json({code: 5, offer: offer })
    }
    catch(e) {
        return res.json({error: constants.SERVER_ERROR})
    }
}


exports.updateOffer = async (req, res, next) => {
    try{
        if(req.file != undefined) {
            let old_file = path.join(__dirname,`../uploads/${req.body.old_picture}`);
            if (fs.existsSync(old_file)) {
                fs.unlinkSync(old_file, (err) =>{
                    if(err) console.log(err);
                    console.log("File deleted");
                });
            }
        }
            
        const offer = {
            offer: req.body.offer,
            picture: req.file ? req.file.filename : req.body.old_picture
        };

        Offer.updateOne({_id:req.params.id},offer,(error,result) => {
            if(error) {
                console.log(error);
                return res.json({code:4, message: constants.ERR_UPDATE_DATA});
            }
            return res.status(200).json({code:5, message: constants.UPDATE_SUCCESS});
        });
    }
    catch(e) {
        console.log(e);
        return res.json({error: constants.SERVER_ERROR})
    }
}



exports.deleteOffer = async (req, res, next) => {
    try{
        let result = await Offer.findOneAndDelete({_id:req.params.id}).lean().catch(e => {
            return res.json({code: 4, message: constants.ERR_REMOVE_DATA })
        });
        console.log(result);
        if(result != null){
            let file = path.join(__dirname,`../uploads/${result.picture}`);
            if (fs.existsSync(file)) {
                fs.unlinkSync(file, (err) =>{
                    if(err) console.log(err);
                    // console.log("File deleted");
                });
            }
        }

        return res.status(200).json({code: 5, message: constants.REMOVE_SUCCESS })
    }
    catch(e) {
        return res.json({error: constants.SERVER_ERROR})
    }
}

