const mongoose = require("mongoose");

const offerSchema = new mongoose.Schema(
    {
        offer: {type: String, required: true},
        picture: {type: String, required: true}
    },
    {
        timestamps: true
    }
);

module.exports = mongoose.model("offers", offerSchema);